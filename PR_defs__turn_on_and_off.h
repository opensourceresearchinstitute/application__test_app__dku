/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR_DEFS_TURN_ON_AND_OFF_H
#define _PR_DEFS_TURN_ON_AND_OFF_H
#define _GNU_SOURCE


#define MODE__MULTI_LANG

//======================  Turn Debug things on and off  =====================
/*When DEBUG__TURN_ON_SEQUENTIAL_MODE is defined, PR does sequential exe in the main thread
 * It still does co-routines and all the mechanisms are the same, it just
 * has only a single thread and animates Slvs one at a time
 */
//#define DEBUG__TURN_ON_SEQUENTIAL_MODE

/*turns on the probe-instrumentation in the application -- when not
 * defined, the calls to the probe functions turn into comments
 */
#define DEBUG__TURN_ON_DEBUG_PRINT

/*These defines turn types of bug messages on and off
 */
#define dbgAppFlow   TRUE /* Top level flow of application code -- general*/
#define dbgProbes    FALSE /* for issues inside probes themselves*/
#define dbgMaster    FALSE /* obsolete*/
#define dbgRqstHdlr  TRUE /* in request handler code*/
#define dbgSS        FALSE /* in request handler code*/
#define dbgInfra     FALSE /* in request handler code*/

//#define DEBUG__TURN_ON_ERROR_MSGS

//==================  Turn Probe Things on and off ====================
/*Probes are used in the application as a cheap, convenient, and fast way
 * to collect statistics.  Define this to enable them, else the probe
 * statements in the application code all turn into empty whitespace
 * in the pre-processor
 */
//#define PROBES__TURN_ON_STATS_PROBES

/*When PROBES__TURN_ON_STATS_PROBES is defined, turn on one of these to choose
 * what kind of measurement the probes store
 */
//#define PROBES__USE_TSC_PROBES
#define PROBES__USE_TIME_OF_DAY_PROBES
//#define PROBES__USE_PERF_CTR_PROBES


//==============  Turn Internal Measurement Things on and off ===============

//#define MEAS__TURN_ON_SUSP_MEAS
//#define MEAS__TURN_ON_MASTER_MEAS
//#define MEAS__TURN_ON_MASTER_LOCK_MEAS
//#define MEAS__TURN_ON_MALLOC_MEAS
//#define MEAS__TURN_ON_PLUGIN_MEAS
//#define MEAS__TURN_ON_SYSTEM_MEAS
   /*turn on/off subtraction of create measurements from plugin meas*/
//#define MEAS__TURN_ON_EXCLUDE_CREATION_TIME 


//#define HOLISTIC__TURN_ON_PERF_COUNTERS
//#define HOLISTIC__TURN_ON_OBSERVE_UCC
//#define HOLISTIC__TURN_ON_DETECT_CONSTRAINT_GRAPH

//===================  Turn on or off system options  =======================
//
/*Defining SYS__TURN_ON_WORK_STEALING causes the core controller behavior
 * to change.  When it detects too many back-to-back masters, then it 
 * searches the other core controllers, looking for work it can steal from
 * them.
 */
//#define SYS__TURN_ON_WORK_STEALING

//===========================================================================
#endif	/*  */

