OpenSourceResearchInstitute.org    Test application for DKU functionality

Summary:

DKU is a programming model for writing portable parallel code.  When employing this pattern, the computation will automatically be divided into pieces of the appropriate size, which fit to the memory hierarchy and number and size of machines.  The size is chosen such that maximum performance is achieved when the application is run.

Advantage  and  Benefit:

The main advantage of DKU is that it works with irregular problems that have complex data structures and complex dependencies.  This contrasts to patterns such as Map-reduce which require highly regular data structures, and require that tasks are fully independent.


How to write code using DKU:

DKU stands for "Divider Kernel Undivider", which are the three principle functional parts of the pattern.  It is normally used with a loop nest.  The user of the pattern writes code that chooses the iteration bounds at particular levels of the loop nest.  Each set of iteration bounds represents an amount of work.  Extensions to the basic DKU pattern allow inserting a communication point within the kernal code.  This point can specify any of the other identical kernel instances to communicate with.  The address of the kernel has a clean parameterization that automatically adjusts when the work is divided. See the paper on the DKU pattern: http://opensourceresearchinstitute.org/uploads/BLIS/MPSoC_DKU.pdf


Building the code:

This is a test application, which is built as an executable.  It has a cmake build script as well as a netbeans project directory.

To compile, you can just cd into the root directory and type:
cmake .
make

This will do an in-source compile.  (Although, the cmake is designed to move the executable to a location that is specific to the proto-runtime development environment.  You will likely want to change the cmake so that it moves the built executable file to a location of your choice)

The cmake is designed to be included in a larger project that is organized for development of the proto-runtime system.  The version of DKU tested here is part of the proto-runtime toolkit and is implemented in the proto-runtime dynamic library.

This repository relies on having the proto-runtime library and the OSRI C utility libraries at a known location, which is encoded in the cmake files.  These libraries can be found at opensourceresearchinstitute.org or on bitbucket under the opensourceresearchinstitute team's repos.

running the test requires also having the proto-runtime repository, and an umbrella that builds everything.  These can all be found in the same two places opensourceresearchinstitute.org or on bitbucket under the opensourceresearchinstitute team's repos.

For questions, email seanhalle@opensourceresearchinstitute.org