/*
 *  Copyright 2013 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 */

#ifndef _DKU_TEST_APP_H_
#define _DKU_TEST_APP_H_

#include <stdio.h>

#include "../PR_defs__turn_on_and_off.h"
#include <PR__include/langlets/PRServ__wrapper_library.h>

/*Bare smoke test of DKU wrapper library functions.
 * Create one DKU instance, with a dummy kernel
 * Bare bones divider and undivider
 * simple root piece maker
 * dummy serial kernel
 */

//===============================  Defines  ==============================

//==============================  Structures  ==============================
typedef struct
 { int32 *data;
 } 
SeedParams;

typedef struct
 {
   int32 *data;
   int32  size;
   int32  startIter;
   int32  endIter;
 }
TestAppStruct;

//============================= Processor Functions =========================
void test_app_seed_Fn(      void *data, SlaveVP *animatingVP ); //seed VP function

DKUPiece *rootPieceMakerFn( void *data, DKUInstance *dkuInstance );
void kernelFn( void *_params, SlaveVP *animVP );       //used as task birth Fn
void serialKernelFn( void *_params, SlaveVP *animVP ); //used as task birth Fn
void dividerFn( DKUPiece *piece );
void undividerFn( DKUPiece *piece );

DKUPiece *
make_root_dku_piece_for_test_inst( int32 *data, int32 size, 
                                   DKUInstance *dkuInstance );

//================================ Global Vars ==============================

#endif /*_SSR_MATRIX_MULT_H_*/
