/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */


#include <math.h>
#include <string.h>
#include "DKU__Test_App.h"

/*Bare smoke test of DKU wrapper library functions.
 * Create one DKU instance, with a dummy kernel
 * Bare bones divider and undivider
 * simple root piece maker
 * dummy serial kernel
 */

int 
square( int x ) 
 { return x*x; 
 }


DKUPiece *
rootPieceMakerFn( void *params, DKUInstance *dkuInstance )
 { DKUPiece      *rootPiece;
 
   rootPiece          = PRServ__DKU_make_empty_DKU_piece();
   rootPiece->parent  = NULL;
   rootPiece->payload = params;
   rootPiece->dkuInstance = dkuInstance;
   
   return rootPiece;
 }

DKUPiece *
make_root_dku_piece_for_test_inst( int32 *data, int32 size, 
                                   DKUInstance *dkuInstance )
 { DKUPiece      *rootPiece;
   TestAppStruct *params;
   
   params = PR__malloc( sizeof(TestAppStruct) );
   params->startIter  = 0;
   params->endIter    = size-1;
   params->data       = data;
   params->size       = size;
   rootPiece = (*(dkuInstance->rootPieceMaker))( params, dkuInstance );
   return rootPiece;
 } 


void 
kernelFn( void  *_params, SlaveVP *animVP )
 { int32 i;
   DKUPiece *piece = (DKUPiece *)_params;
   TestAppStruct *params = (TestAppStruct *)piece->payload;
   int32 *data           = params->data;
   
         DEBUG__printf(dbgAppFlow, "Kernel %d", params->startIter);
   
   for( i=params->startIter; i <= params->endIter; ++i ) 
    { data[i] = square(i);
    }
   PRServ__DKU_end_kernel( piece, animVP );
 }


/*Serial kernel is called with bare app-defined struct, not DKUPiece
 */
void 
serialKernelFn( void *_params, SlaveVP *animVP )
 { int32 i;
   TestAppStruct *params = (TestAppStruct *)_params;
   int32 *data = params->data;
   
         DEBUG__printf(dbgAppFlow, "SerialKernel %d", params->startIter);
   
   for( i=params->startIter; i < params->endIter; ++i ) 
    { data[i] = square(i);
    }
   PRServ__DKU_end_serial_kernel( animVP );
 }

void
dividerFn( DKUPiece *pieceToDivide )
 {
   int32 idx, childIdx, numChildren, size, *data;
   DKUPiece *childPiece, **childrenArray;
   TestAppStruct *params;
         DEBUG__printf(dbgAppFlow, "divider %p", pieceToDivide)
   
   numChildren = pieceToDivide->numChildren;
   childrenArray = PR__malloc( numChildren * sizeof(DKUPiece *) );
   size = ((TestAppStruct*)pieceToDivide->payload)->size;
   data = ((TestAppStruct*)pieceToDivide->payload)->data;
   int chunkSize = size/numChildren; //for portability, lang would choose this dynamically at run time
   //note: should do processing to catch too small a chunksize and deal w/it
   for( childIdx = 0; childIdx < numChildren; childIdx++ ) 
    { params = PR__malloc(sizeof(TestAppStruct)); //these define work the task performs
      idx = childIdx * chunkSize;
      params->startIter = idx;
      params->endIter   = idx + chunkSize -1;
      params->data      = data;
      params->size      = size;
      childPiece = PRServ__DKU_make_child_piece_from( pieceToDivide );
      childPiece->payload = params;
      childrenArray[childIdx] = childPiece;
    }
   params->endIter = size -1; //catch truncation error from chunkSize calc
   
   pieceToDivide->children = childrenArray;
 }

void undividerFn( DKUPiece *piece )
 {
         //DEBUG__printf(dbgAppFlow, "Undivider %p", piece)
   //nothing to do -- request handler does counting of completions 
 }

