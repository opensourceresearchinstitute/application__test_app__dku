/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */


#include <math.h>
#include <string.h>
#include "DKU__Test_App.h"

/*Bare smoke test of DKU wrapper library functions.
 * Create one DKU instance, with a dummy kernel
 * Bare bones divider and undivider
 * simple root piece maker
 * dummy serial kernel
 */

//====================================================================
#define NO_INPUT NULL
 /*Just to get proto-runtime built and run, to test it..
  */
void test_app_seed_Fn( void *_params, SlaveVP *seedVP )
 { DKUInstance *dkuInstance;
   DKUPiece    *rootPiece;
   int32  size = 1000;
   int32 *data = (int32 *) PR__malloc (size * sizeof(int32)) ;
   
         DEBUG__printf(dbgAppFlow, "In seed Fn")
       
   SeedParams *seedParams = (SeedParams *)_params; //used to comm with main()
   
   //A DKU Instance is the unit that isolates the use of DKU in one place, from another.  IE, can do 5 matrix multiplies
   // in the program.. each has to be isolated from the others, with its own state.  Thus, need an array of root pieces, 
   // and meta-data for each root piece.  Making a DKU Instance reserves the index and creates the meta-data.
   // Thus, the DKU Instance has to be passed around, each time do something DKU specific.
   //Note that this is the wrong model.  DKU should be inside the PR scheduling, and the application should not
   // see any of this..  Rather, the language lib should just register the functions as a "type" and then mark
   // new VPs or new tasks as being candidates for DKU, with the type.  Then inside PR, the scheduler creates
   // the instance, meta-data, etc.
   dkuInstance = PRServ__DKU_make_empty_DKU_instance( seedVP );
   PRServ__DKU_set_root_piece_maker( dkuInstance, &rootPieceMakerFn, seedVP );
   PRServ__DKU_set_kernel( dkuInstance, &kernelFn, seedVP );
   PRServ__DKU_set_serial_kernel( dkuInstance, &serialKernelFn, seedVP );
   PRServ__DKU_set_divider( dkuInstance, &dividerFn, seedVP );
   PRServ__DKU_set_undivider( dkuInstance, &undividerFn, seedVP );
   
   rootPiece = 
       make_root_dku_piece_for_test_inst( data, size, dkuInstance );
//   rootPiece = PRServ__DKU_make_root_piece( dkuInstance, data, seedVP );
   
   PRServ__DKU_perform_work_on( rootPiece, seedVP );
   
   PRServ__DKU_wait_for_result_to_be_complete( rootPiece, seedVP );

   seedParams->data = data; //sends results back to main()
   
      //Tells PR to end the process, which it will do even
      // if work is active, or suspended work entities are still live, or the
      // process has input ports that could trigger future work.
   PR__end_process_from_inside( seedVP );

      //This ends the last live entity capable of work, in a process
      // that has no external input ports.. hence, no activity can take place
      // past that point..  PR detects that, and then automatically ends the
      // process.
   PR__end_seedVP( seedVP );
 }

