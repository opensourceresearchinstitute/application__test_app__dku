## Variables for CPack Debian packaging

set(CPACK_PACKAGE_DESCRIPTION "Utility Libraries")
include(CPack)

cpack_add_component(application
                     DISPLAY_NAME test_app_dku
                     DESCRIPTION Test application for DKU functionality
                     REQUIRED
                     DEPENDS lib_pr lib_util_c
                     DOWNLOADED)
